(def map_data [])
(def hasFound false)

(defn print_current_map [] 
	(doseq [line map_data]
		(println line)
	)
)

(defn checkAndChangeExclamatory [row, column]
	(def add_symbol_count 0)
	(def row_size (count map_data))
	(def column_size (count(get map_data 0)))
	(if (not hasFound)
		(do
			(if (and (>= (- row 1) 0) (= (get (get map_data (- row 1)) column) \+))
				(do
					(def add_symbol_count (inc add_symbol_count))
				)
			)
			(if	(and (>= (- column 1) 0) (= (get (get map_data row) (- column 1)) \+))
				(do
					(def add_symbol_count (inc add_symbol_count))
				)
			)
			
			(if	(and (< (+ row 1) row_size) (= (get (get map_data (+ row 1)) column) \+))
				(do
					(def add_symbol_count (inc add_symbol_count))
				)
			)
			(if (and (< (+ column 1) column_size) (= (get (get map_data row) (+ column 1)) \+))
				(do
					(def add_symbol_count (inc add_symbol_count))
				)
			)
			(if (< add_symbol_count 2)
				(do
					(def row_update (get map_data row))
					(def row_update (str (subs row_update 0 column) (str "!" (subs row_update (+ column 1)(count row_update)))))
					(def map_data (assoc map_data row row_update))
				)
			)
		)
	)
)

(defn isValidIndex [row, column]
	(def row_size (count map_data))
	(def column_size (count(get map_data 0)))
	(if(and (and (and (>= row 0) (< row row_size)) (>= column 0)) (< column column_size))
		(do
			reduce true
		)
		(do
			reduce false
		)
	)
)

(defn findPath [row, column]
	(if (and (not hasFound)(and (and (isValidIndex row column) (not= (get (get map_data row) column) \#)) (not= (get (get map_data row) column) \+)))
		(do
			(cond
				(= (get (get map_data row) column) \@)
					(do
						(def hasFound true)
					)
				(= (get (get map_data row) column) \-)
					(do
						(def row_update (get map_data row))
						(def row_update (str (subs row_update 0 column) (str "+" (subs row_update (+ column 1)(count row_update)))))
						(def map_data (assoc map_data row row_update))
					)
				(= (get (get map_data row) column) \+)
					(do
						(def row_update (get map_data row))
						(def row_update (str (subs row_update 0 column) (str "!" (subs row_update (+ column 1)(count row_update)))))
						(def map_data (assoc map_data row row_update))
					)
			)
			(findPath (- row 1) column)
			(findPath row, (- column 1))
			(findPath (+ row 1) column)
			(findPath row, (+ column 1))
			(checkAndChangeExclamatory row column)
		)
	)
)

(defn validate_map
	"Validates the map and return true or false"
	[]
	(def column_count (count(get map_data 0)))
	(doseq [line map_data]
		(if (not= column_count (count line))
			reduce false
		)
		(doseq [character line]
			(if (= character "#")
				(do
					(println character)
				)
			)
		)
	)
	reduce true
)

(defn treasure_main 
	"The treasure_main is the main class from where the program starts"
	[]
	(if (.exists (clojure.java.io/file "map.txt"))
		(do
			(with-open [rdr (clojure.java.io/reader "map.txt")]
				(doseq [line (line-seq rdr)]
					(def map_data (conj map_data line))
				)
			)
		)
		(do
			(println "File Not Found")
		)
	)

	(def valid_map true)
	(def column_count (count(get map_data 0)))
	(doseq [line map_data]
		(if valid_map
			(do
				(if (not= column_count (count line))
					(do
						(def valid_map false)
					)
				)
				(doseq [character line]
					(if	(and (and (not= character \#) (not= character \-)) (not= character \@))
						(do
							(def valid_map false)
						)
					)
				)
			)
		)
	)
	
	(if valid_map
		(do
			(println "This is my challenge")
			(print_current_map)
			(findPath 0 0)
			(if hasFound
				(do
					(println "Treasure Found")
					(print_current_map)
				)
				(do
					(println "Treasure Not Found")
					(print_current_map)
				)
			)
		)
		(do
			(println "Invalid File")
		)
	)
	
)
(treasure_main)